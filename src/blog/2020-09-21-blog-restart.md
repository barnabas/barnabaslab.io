---
title: Blog Restart
---

With this post, I'm restarting my blog.<!-- more -->
Maybe the ["articles"](/articles/) format was too ambitious and keeping me from writing more often.
At least, that's the most self-sparing explanation.
I say "restart" because I've blogged before on Wordpress and Tumblr, but not recently.

Technically speaking, I enabled a Vuepress [plugin](https://vuepress-plugin-blog.ulivz.com/) and embedded this blog into the existing site.
If you're interested, the [source for this site is public](https://gitlab.com/barnabas/barnabas.gitlab.io),
and the configuration to make this work is mostly in [docs/.vuepress/config.js](https://gitlab.com/barnabas/barnabas.gitlab.io/-/blob/master/docs/.vuepress/config.js).

Personally speaking, I'm glad to have a place to journal again.
I'm also thankful my only recurring cost is the annual domain name renewal.
