---
title: Memory Article
---

Last week I repeatedly had a sense of _déjà vu_, especially whenever I would begin a new email or text message.
It happened frequently enough that I began to worry if I was losing my memory.
So, I started writing a blog post about how vital a good memory is, but before I knew it I had written far too much. 
So I put it [in a separate article](../articles/memory.md) and then I kept fussing with it for too long.

But then I remembered that articles are meant to be updated continuously, so out it goes.
Now I just have to remember to update it later.
