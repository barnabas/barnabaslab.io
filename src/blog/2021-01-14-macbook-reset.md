---
title: Macbook Hard Drive Reset
---

The other week my MacBook Pro's hard disk died.
Even though it was one day past warranty, the Apple Store technician was kind enough to fix it free of charge.
I learned the following things through this experience: <!-- more -->

1. If you pay for your own Apple hardware (as in, not under the protection of an IT department), 
   _you have no choice but to purchase AppleCare_. 
   "Ah but I do have a choice," you say, "or why would they let me decline coverage?"
   True. You also have the choice to cover yourself in Worcestershire sauce and swim in a tank of piranhas.
1. The cost to you to replace a 512 GB hard disk for a 2017 MacBook Pro is $740 + 1 week of No Laptop.
   The cost to your spouse is arguably much higher, if you are the complainy type.
1. Apple Stores' pandemic protocol is very good: plexiglass, limited customers allowed inside, constant surface wiping.
1. Being inside a nearly empty Apple Store is eerie and sad. The music doesn't help.
1. Having all of your code in source control and your files in the cloud is not the same thing as having your computer properly backed up.
1. An iPad is no substitute for a laptop. Not even close.
1. Hardware problems can really sour one's mood.
1. Even if you tell the technician that you want macOS Catalina not Big Sur, 
   you should double-check that they didn't helpfully install Big Sur anyway.
1. To downgrade macOS, you'll need a blank USB flash drive, [these instructions](https://support.apple.com/en-us/HT201372), 
   and an abundance of calm serenity.
1. Modern software and hardware is simultaneously an astonishing miracle and also the worst thing ever.