import { defineClientAppEnhance } from '@vuepress/client';
import "@fontsource/nunito";

const priceFmt = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' })

export default defineClientAppEnhance(({ app, router, siteData }) => {
  app.config.globalProperties.$filters = {
    currencyUSD(value) {
      return isNaN(value) ? 'N/A' : priceFmt.format(value)
    },
    dateLong(timestamp) {
      const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', timeZone: 'UTC' }
      return new Date(timestamp).toLocaleString('en-us', options)
    }
  }
})
