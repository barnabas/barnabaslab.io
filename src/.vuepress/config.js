const path = require('path');

module.exports = {
    lang: 'en-US',
    title: 'Barnabas Kendall',
    description: 'My personal website',
    dest: "public", // required for gitlab pages
    extendsMarkdown: (md) => {
        md.set({ typographer: true })
        md.use(require('markdown-it-footnote'))
    },
    extendsPageOptions: ({ filePath }, app) => {
        if (!filePath?.endsWith('README.md') && filePath?.startsWith(app.dir.source('blog/'))) {
            return {
                frontmatter: {
                    permalinkPattern: 'blog/:year/:month/:slug.html',
                    sidebar: false,
                    layout: 'BlogLayout',
                },
            }
        }
        return {}
    },
    plugins: [
        [
            '@vuepress/plugin-search',
            {
                // make full-text searchable
                getExtraFields: (page) => [page.content],
                isSearchable: (page) => page.path !== '/' && page?.frontmatter.searchable !== false,
            },
        ],
        [
            '@vuepress/register-components',
            {
                componentsDir: path.resolve(__dirname, './components'),
            },
        ],
        {
            async onPrepared(app) {
                const pageData = app.pages
                    .filter(p => p.title !== '')
                    .map(p => {
                        const { date } = p
                        const { path, title, excerpt, git, headers } = p.data;
                        const { description } = p.frontmatter;
                        const lastUpdated = new Date(git.updatedTime);
                        return { path, title, date, excerpt, lastUpdated, headers, description };
                    });

                pageData.sort((a, b) => a.lastUpdated - b.lastUpdated);

                await app.writeTemp('pageData.js', `const pageData = ${JSON.stringify(pageData)}; export default pageData;`);

                function getLatest(matchPath) {
                    const matching = pageData.filter(p => p.path.startsWith(matchPath) && p.path !== matchPath);
                    return matching[matching.length - 1];
                }

                const latest = {
                    blogPost: getLatest('/blog/'),
                    article: getLatest('/articles/')
                }
                await app.writeTemp('latest.js', `const latest = ${JSON.stringify(latest)}; export default latest;`);
            }
        }
    ],
    themeConfig: {
        contributors: false,
        navbar: [
            { text: 'Articles', link: '/articles/' },
            { text: 'Blog', link: '/blog/' },
            { text: 'Projects', link: 'https://gitlab.com/barnabas' }
        ],
        sidebar: 'auto',
    }
};

