class BasePlan {
  constructor (options = {}) {
    this.name = options.name
    this.memoryGB = options.memoryGB || null
    this.cores = options.cores || null
    this.price = options.price || null
  }

  matches (options) {
    if (!options) return false
    return (this.memoryGB === null || this.memoryGB >= options.memoryGB) &&
      (this.cores === null || this.cores >= options.cores)
  }

  get coresLabel () {
    return this.cores === 1 ? '1 core' : this.cores + ' cores'
  }
}

class DynamicPlan extends BasePlan {
  constructor (options = {}) {
    super(options)
    this.storagePrice = options.storagePrice
    this.freeStorageGB = Math.max(0, options.freeStorageGB || 0)
  }

  fullPrice (storageGB) {
    if(storageGB <= this.freeStorageGB) return this.price
    return this.price + ((storageGB - this.freeStorageGB) * this.storagePrice)
  }

  getStaticPlan (budget) {
    const remaining = budget - this.price
    const storageGB = (remaining < 0) ? 0 : Math.floor(remaining / this.storagePrice) + this.freeStorageGB

    return new StaticPlan({ ...this, storageGB })
  }

  get description () {
    return `${this.memoryGB} GB memory, ${this.coresLabel}`
  }
}

// for providers with set plan tiers
class StaticPlan extends BasePlan {
  constructor (options = {}) {
    super(options)
    this.storageGB = options.storageGB || null
  }

  matches (options) {
    return super.matches(options) &&
      (this.storageGB === null || this.storageGB >= options.storageGB)
  }

  get description () {
    return `${this.memoryGB} GB memory, ${this.storageGB} GB storage` + (this.cores > 0 ? `, ${this.coresLabel}` : '')
  }
}

const PLANS_AWS = [
  { name: 'db.t3.micro', memoryGB: 1, cores: 2, price: 9.417 },
  { name: 'db.t3.small', memoryGB: 2, cores: 2, price: 18.834 },
  { name: 'db.t3.medium', memoryGB: 4, cores: 2, price: 37.741 },
  { name: 'db.t3.large', memoryGB: 8, cores: 2, price: 75.482 },
  { name: 'db.t3.xlarge', memoryGB: 16, cores: 4, price: 150.964 },
  { name: 'db.t3.2xlarge', memoryGB: 32, cores: 8, price: 301.855 },
  { name: 'db.r5.xlarge', memoryGB: 32, cores: 4, price: 210.824 },
  { name: 'db.r5.2xlarge', memoryGB: 64, cores: 8, price: 421.575 },
  { name: 'db.r5.4xlarge', memoryGB: 128, cores: 16, price: 843.150 },
].map(options => new DynamicPlan({ storagePrice: 0.138, ...options }))

const PLANS_AZURE = [
  { name: 'Basic Gen 5', cores: 1, memoryGB: 2, price: 24.82, storagePrice: 0.10 },
  { name: 'Basic Gen 5', cores: 2, memoryGB: 4, price: 49.64, storagePrice: 0.10 },
  { name: 'General Purpose Gen 5', cores: 2, memoryGB: 10, price: 77.672, storagePrice: 0.115 },
  { name: 'General Purpose Gen 5', cores: 4, memoryGB: 20, price: 155.344, storagePrice: 0.115 },
  { name: 'General Purpose Gen 5', cores: 8, memoryGB: 40, price: 310.688, storagePrice: 0.115 },
  { name: 'General Purpose Gen 5', cores: 16, memoryGB: 80, price: 621.376, storagePrice: 0.115 }
].map(options => new DynamicPlan(options))

const PLANS_DO = [
  { memoryGB: 1, cores: 1, storageGB: 10, price: 15 },
  { memoryGB: 2, cores: 1, storageGB: 25, price: 30 },
  { memoryGB: 4, cores: 2, storageGB: 38, price: 60 },
  { memoryGB: 8, cores: 4, storageGB: 115, price: 120 },
  { memoryGB: 16, cores: 6, storageGB: 270, price: 240 },
  { memoryGB: 32, cores: 8, storageGB: 580, price: 480 },
  { memoryGB: 64, cores: 16, storageGB: 1120, price: 1600 },
].map(options => new StaticPlan(options))

const PLANS_ELEPHANT = [
  { name: 'Blissful Butterfly', memoryGB: 2, storageGB: 50, price: 49 },
  { name: 'Happy Hippo', memoryGB: 4, storageGB: 100, price: 99 },
  { name: 'Enormous Elephant', memoryGB: 8, storageGB: 250, price: 199 },
  { name: 'Puffy Pigeon', memoryGB: 16, storageGB: 500, price: 399 },
  { name: 'Dramatic Dolphin', memoryGB: 32, storageGB: 1000, price: 749 },
  { name: 'Ruthless Rat', memoryGB: 64, storageGB: 2000, price: 1399 },
].map(options => new StaticPlan(options))

const PLANS_FLY_IO = [
  { name: "shared-cpu-1x", cores: 1, memoryGB: 0.25, price: 1.94},
  { name: "shared-cpu-1x", cores: 1, memoryGB: 1, price: 5.70},
  { name: "dedicated-cpu-1x", cores: 1, memoryGB: 2, price: 31},
  { name: "dedicated-cpu-1x", cores: 1, memoryGB: 4, price: 41.01},
  { name: "dedicated-cpu-1x", cores: 1, memoryGB: 8, price: 61.02},
  { name: "dedicated-cpu-2x", cores: 2, memoryGB: 4, price: 62},
  { name: "dedicated-cpu-2x", cores: 2, memoryGB: 8, price: 92.02},
  { name: "dedicated-cpu-2x", cores: 2, memoryGB: 16, price: 132.04},
  { name: "dedicated-cpu-4x", cores: 4, memoryGB: 8, price: 124},
  { name: "dedicated-cpu-4x", cores: 4, memoryGB: 16, price: 194.04},
  { name: "dedicated-cpu-4x", cores: 4, memoryGB: 32, price: 274.08},
  { name: "dedicated-cpu-8x", cores: 8, memoryGB: 16, price: 248},
  { name: "dedicated-cpu-8x", cores: 8, memoryGB: 32, price: 398.08},
  { name: "dedicated-cpu-8x", cores: 8, memoryGB: 64, price: 558.16},
].map(options => new DynamicPlan({ storagePrice: 0.15, freeStorageGB: 3, ...options }))

const PLANS_HEROKU = [
  { name: 'Standard 0', memoryGB: 4, storageGB: 64, price: 50 },
  { name: 'Standard 2', memoryGB: 8, storageGB: 256, price: 200 },
  { name: 'Standard 3', memoryGB: 15, storageGB: 512, price: 400 },
  { name: 'Standard 4', memoryGB: 30, storageGB: 750, price: 750 },
  { name: 'Standard 5', memoryGB: 61, storageGB: 1000, price: 1400 },
  { name: 'Standard 6', memoryGB: 122, storageGB: 1500, price: 2000 },
].map(options => new StaticPlan(options))

const PLANS_LIGHTSAIL = [
  { name: '$15 Standard', memoryGB: 1, cores: 1, storageGB: 40, price: 15 },
  { name: '$30 Standard', memoryGB: 2, cores: 1, storageGB: 80, price: 30 },
  { name: '$60 Standard', memoryGB: 4, cores: 2, storageGB: 120, price: 60 },
  { name: '$115 Standard', memoryGB: 8, cores: 2, storageGB: 240, price: 115 },
].map(options => new StaticPlan(options))

const PLANS_GCP = PLANS_DO.map(plan => getGCPPlan(plan.memoryGB, plan.cores)) // not real offerings, just using DO as model

const PLANS_SCALEGRID = [
  { name: 'Micro', memoryGB: 1, cores: 1, storageGB: 10, price: 20 },
  { name: 'Small', memoryGB: 2, cores: 1, storageGB: 40, price: 40 },
  { name: 'Medium', memoryGB: 4, cores: 1, storageGB: 60, price: 87 },
  { name: 'Large', memoryGB: 8, cores: 2, storageGB: 120, price: 191 },
  { name: 'XLarge', memoryGB: 16, cores: 2, storageGB: 240, price: 302 },
  { name: 'X2XLarge', memoryGB: 32, cores: 4, storageGB: 480, price: 605 },
  { name: 'X4XLarge', memoryGB: 64, cores: 8, storageGB: 700, price: 1178 }
].map(options => new StaticPlan(options))

const PLANS_RENDER = [
  { name: 'Starter', memoryGB: 0.25, cores: 1, storageGB: 1, price: 7 },
  { name: 'Standard', memoryGB: 1, cores: 1, storageGB: 16, price: 20 },
  { name: 'Standard Plus', memoryGB: 2, cores: 1, storageGB: 48, price: 45 },
  { name: 'Pro', memoryGB: 4, cores: 2, storageGB: 96, price: 96 },
  { name: 'Pro Plus', memoryGB: 8, cores: 4, storageGB: 256, price: 185 },
].map(options => new StaticPlan(options))

const BLANK_PLAN = { description: 'N/A', fullPrice: () => null }

const getPlan = (plans, options) => plans.find(plan => plan.matches(options)) || BLANK_PLAN

export const getAWSPlan = (memoryGB, cores) => getPlan(PLANS_AWS, { memoryGB, cores })

export const getAzurePlan = (memoryGB, cores) => getPlan(PLANS_AZURE, { memoryGB, cores })

export const getDigitalOceanPlan = (memoryGB, storageGB, cores) => getPlan(PLANS_DO, { memoryGB, storageGB, cores })

export const getElephantPlan = (memoryGB, storageGB, cores) => getPlan(PLANS_ELEPHANT, { memoryGB, storageGB, cores })

export function getGCPPlan (memoryGB, cores) {
  const coreHour = 0.0413 * cores
  const memoryHour = 0.0070 * memoryGB
  const price = ((coreHour + memoryHour) * 730) // approx. hours per month

  return new DynamicPlan({ memoryGB, cores, price, storagePrice: 0.170 })
}

export const getFlyIOPlan = (memoryGB, cores) => getPlan(PLANS_FLY_IO, { memoryGB, cores })

export const getHerokuPlan = (memoryGB, storageGB) => getPlan(PLANS_HEROKU, { memoryGB, storageGB })

export const getLightsailPlan = (memoryGB, storageGB, cores) => getPlan(PLANS_LIGHTSAIL, { memoryGB, storageGB, cores })

export const getScaleGridPlan = (memoryGB, storageGB, cores) => getPlan(PLANS_SCALEGRID, { memoryGB, storageGB, cores })

export const getRenderPlan = (memoryGB, storageGB, cores) => getPlan(PLANS_RENDER, { memoryGB, storageGB, cores })

function getPlanByBudget (plans, budget) {
  for (let i = plans.length - 1; i > -1; i--) {
    if (plans[i].price <= budget) return plans[i]
  }
}

function getStaticPlanByBudget (plans, budget) {
  const plan = getPlanByBudget(plans, budget)
  return plan ? plan.getStaticPlan(budget) : BLANK_PLAN
}

export const budgetAWS = budget => getStaticPlanByBudget(PLANS_AWS, budget)

export const budgetAzure = budget => getStaticPlanByBudget(PLANS_AZURE, budget)

export const budgetGCP = budget => getStaticPlanByBudget(PLANS_GCP, budget)

export const budgetDigitalOcean = budget => getPlanByBudget(PLANS_DO, budget) || BLANK_PLAN

export const budgetElephant = budget => getPlanByBudget(PLANS_ELEPHANT, budget) || BLANK_PLAN

export const budgetFlyIO = budget => getStaticPlanByBudget(PLANS_FLY_IO, budget);

export const budgetHeroku = budget => getPlanByBudget(PLANS_HEROKU, budget) || BLANK_PLAN

export const budgetLightsail = budget => getPlanByBudget(PLANS_LIGHTSAIL, budget) || BLANK_PLAN

export const budgetScaleGrid = budget => getPlanByBudget(PLANS_SCALEGRID, budget) || BLANK_PLAN

export const budgetRender = budget => getPlanByBudget(PLANS_RENDER, budget) || BLANK_PLAN
