---
description: This is a comparison of PostgreSQL DBaaS providers with an interactive calculator.
---
# PostgreSQL DBaaS Calculator

I recently wanted to start a side-project using the serverless application hosting service ~~Zeit Now~~ [Vercel](https://vercel.com/home).
Under "how do I use databases" in their FAQ, they recommend cloud databases such as Azure CosmosDB, MongoDB Atlas, Firebase, and Google Cloud SQL.
Personally, I want a relational database like PostgreSQL, and nowadays there are lots of options.

While it's certainly _possible_ to self-host a PostgreSQL DB on a DigitalOcean droplet for $5/month, that doesn't mean it's a good idea.
In my opinion, outsourcing DB maintenance to a vendor is a no-brainer until your hosting bill is in the triple digits.
Most side-projects will never get that far.

This page is an effort to compare DBaaS vendors for hosting a PostgreSQL database for my personal projects.
My focus is on these smaller side-project type deployments (< $50/month or so), not giant enterprise-level clusters.
There are links to the pricing pages too (:moneybag:).
If you find an error or have a suggestion, please [open an issue](https://gitlab.com/barnabas/barnabas.gitlab.io/issues)
or better yet [submit a merge request](https://gitlab.com/barnabas/barnabas.gitlab.io/merge_requests).
Here is an old [HN discussion](https://news.ycombinator.com/item?id=19534247).
Thanks!

## Vendor Overview

### [Amazon RDS for PostgreSQL](https://aws.amazon.com/rds/postgresql/)

I have used RDS for PostgreSQL at work and I have no complaints.
There are several instance types that have two vCPUs and vary by memory, but as soon as you need more CPU, you move to db.t3.xlarge.
Amazon's [T3 DB instance types use some kind of CPU bursting and CPU credit system](https://aws.amazon.com/rds/instance-types/), and yet DBaaS was supposed to be _easier_ somehow...
The calculator assumes a 1-year commitment of reserved instances with no upfront payment in US West (Oregon).
[:moneybag:](https://aws.amazon.com/rds/postgresql/pricing/)

### [Amazon Lightsail](https://aws.amazon.com/lightsail/)

Lightsail is Amazon's simplified version of AWS.
Although at launch only MySQL was available, they quietly added managed PostgreSQL at some point in early 2019 (I think).
It seems that the prices are very similar to what you can get with a 1-year reserved RDS.
Lightsail seems like a good choice early in a project if you're leaning towards AWS but you're not ready for a 1-year reservation.
They're currently giving away [3 months free on the $15/month DB bundle](https://aws.amazon.com/lightsail/pricing/?loc=ft#AWS_Free_Tier).
[:moneybag:](https://aws.amazon.com/lightsail/pricing/)

### [Azure Database for PostgreSQL](https://azure.microsoft.com/en-us/services/postgresql/)

Azure does not break out their pricing for memory; you pick the machine class based on CPU and pay for storage.
This calculator starts applying the 1-year 39% discount once you select General Purpose Compute Gen 5.
Not reflected in this calculator is the fact that you get [free credits](https://azure.microsoft.com/en-us/free/) to use 750 hours of the flexible server preview for the first 12 months of service.
[:moneybag:](https://azure.microsoft.com/en-us/pricing/details/postgresql/)

### [DigitalOcean Managed Databases](https://www.digitalocean.com/products/managed-databases/)

DO is a relative newcomer to the PostgreSQL hosting world.
While they have their own data centers (presumably), their pricing resembles other tier-2 vendors.
[:moneybag:](https://www.digitalocean.com/pricing/#Databases)

### [ElephantSQL](https://www.elephantsql.com/)

ElephantSQL provides managed PostgreSQL hosting in a variety of other cloud platforms' data centers, including AWS, Softlayer, GCP, and Azure.
Here's another vendor that doesn't break out CPU and I wish they would at least mention it on the plan page.
As a plus, their service level plan names are downright adorable.
Only the dedicated plans are included in this calculator, but they also offer shared instance plans at the free, $5, $10, and $19/month level too.
[:moneybag:](https://www.elephantsql.com/plans.html)

### [Fly.io](https://fly.io/docs/reference/postgres/)

Fly.io is a global application distribution platform that runs code in [Firecracker microVMs](https://firecracker-microvm.github.io/).
Postgres on Fly is a regular fly app, just with extensions to simplify management.
It relies on building blocks available to all fly apps, like volumes, private networking, health checks, logs, metrics, and so on.
They offer [a generous free tier that they described in the announcement](https://fly.io/blog/free-postgres/).
You could actually run a two-node HA cluster using the shared CPU 256MB RAM instances for $0 every month.
[:moneybag:](https://fly.io/docs/about/pricing/#postgresql-clusters)

### [Google Cloud SQL for PostgreSQL](https://cloud.google.com/sql/docs/postgres/)

Google's cloud offering is month-to-month, and they offer a discount for [sustained use](https://cloud.google.com/compute/docs/sustained-use-discounts#sud_table).
You are not tied to specific instance types; instead CPU, storage, and memory are all adjustable.
This makes a great deal of sense to me.
On the other hand, I suspect the Google pricing is inflated below because I can't figure out how [committed use discounts work](https://cloud.google.com/compute/docs/instances/signing-up-committed-use-discounts).
[:moneybag:](https://cloud.google.com/sql/docs/postgres/pricing)

### [Heroku Postgres](https://www.heroku.com/postgres)

Heroku is one of the few vendors with an OK free tier for Postgres.
Unlike Azure, Heroku has pricing tiers based on memory, storage, and number of connections.
CPU is not visible or configurable, so the calculator below may not tell the whole story.
There is a point at the 4 GB memory and 64 GB storage mark where Heroku is almost the best choice at $50/month.
Otherwise Heroku is fairly expensive, per their reputation.
[:moneybag:](https://www.heroku.com/pricing#databases)

### [Render](https://render.com/)

Render is a young PaaS going toe-to-to with Heroku.
Although their "Starter" size is tiny, maybe 256 MB of RAM and 1 GB of storage is enough for your project.
For $7/month and [free for 90 days](https://render.com/docs/free#free-postgresql-databases), it's hard to beat.
Since you can also host services on their platform (also starting at $7/month), you could spin up a Graphile or Hasura backend for $14.
What a time to be alive.
[:moneybag:](https://render.com/pricing#databases)

### [ScaleGrid](https://scalegrid.io/postgresql.html)

ScaleGrid hosts a variety of databases (Mongo, Redis, MySQL, PostgreSQL) on top of a variety of clouds (AWS, Azure, DO).
They offer [unmatched admin control and SSH root access](https://scalegrid.io/postgresql/hosting-comparison.html).
This calculator is using their AWS Standalone pricing for dedicated hosting.
[:moneybag:](https://scalegrid.io/pricing.html?db=POSTGRESQL&cloud=cloud_aws_standard&replica=deployment_standalone&instance=Micro#section_pricing_dedicated)

## Calculator

Here's a calculator to give a rough estimate of monthly costs for each major DBaaS vendor.
This does not factor in cost of network egress, backups or replicas; it's just for basic apples-to-apples between these services.

<ClientOnly>
  <DbaasCalculator />
</ClientOnly>

## Budget

*How Far Does That Dollar Go?*

<ClientOnly>
  <DbaasBudgeter />
</ClientOnly>

Note: the budgeter does not reflect the free tiers of [ElephantSQL](#elephansql), [Fly.io](#fly-io), or [Heroku](#heroku-postgres).

## Observations

There are two clear tiers of DBaaS providers: those who run PostgreSQL on their own metal (AWS, Azure, DO, Fly.io, GCP) and those who rent from the first group.
Azure's 1-year commitment discount gives them an edge at the high end.
Fly.io does well at the very low end and also at the 2 CPU / 16 GB mark and the 4 CPU / 32 GB mark.
Google seems to do well when you max out memory and leave everything else low.
DigitalOcean's sweet spot is their 4 CPU / 8 GB memory / 115 GB storage product.

The smaller vendors cost more but provide value in other ways, such better tooling or support.
Especially at the $100/month range that may really be worth it.
I just can't understand why there's a conspicuous hole in Heroku's lineup between Standard 0 and 2.
Standard 1 must have had 6 GB memory and 128 GB storage for $100. That would have rocked.

### Further Reading

* [Comparison of Managed PostgreSQL Providers](https://hasura.io/blog/comparison-of-managed-postgresql-aws-rds-google-cloud-sql-azure-postgresql/) (2021)
* [Comparing Cloud Database Options for PostgreSQL](https://severalnines.com/database-blog/comparing-cloud-database-options-postgresql) (2018)
* [Pros and Cons of DBaaS](https://www.objectrocket.com/blog/company/advantages-and-disadvantages-of-dbaas/) (2017)

### Other Providers

* [IBM Cloud Databases for PostgreSQL](https://www.ibm.com/cloud/databases-for-postgresql): not included in the calculator because the pricing model is so different, but it starts at 5GB of Disk and 1GB of RAM for $15.80/month.
* [Aiven for PostgreSQL](https://aiven.io/postgresql): similar concept to ScaleGrid

## Changelog

* 2019-03-31: initial version
* 2019-04-01: IBM Cloud
* 2019-10-25: add budget calculator
* 2020-03-08: adjust presets, added ScaleGrid, updated AWS and Azure prices
* 2020-06-28: added Amazon Lightsail
* 2020-10-06: added Render
* 2022-01-23: added Fly.io, called out free tiers
