---
description: Our ability to store and retrieve information reliably is a precious, limited resource.
---
# The Importance of Memory

::: tip An Experiment
Before continuing with this article, as an experiment, try to memorize this short list of words:
<MemoryTest />.
In about two minutes, you'll be asked to recall this list to see how you did.
:::

I am both fascinated and terrified by the importance of a good memory.
It's hard for me to imagine being effective in _any_ job while suffering from a memory impairment.
That's why stories like [this one about a man who lost the ability to record new memories after a trip to the dentist](https://www.bbc.com/future/article/20150630-my-dentist-saved-my-tooth-but-stole-my-memory) are unnerving.

Last week I repeatedly had a sense of _déjà vu_[^1] whenever I would begin a new email or text message. _Didn't I already send this?_ I'd think.
One possible explanation for _déjà vu_ is a [memory reconstruction error](https://en.wikipedia.org/wiki/D%C3%A9j%C3%A0_vu#Memory-based_explanation).
I started keeping some notes to organize my thoughts and that turned into what you're reading now.
I'd like to cultivate empathy for people with memory troubles, including my future self.
Memory is a precious, limited resource.

I began this article during the fall of the 2020 COVID-19 pandemic, a year many would rather forget.
We've all experienced how persistent stress negatively affects our ability to concentrate and remember things.
Many now work from home, trying hard to perform at their usual level.
But, we all have days when everything seems to take longer than before. New concepts are slippery. Concentration weakens.
Could that be the sound of the grinding gears of our stressed-out memory machinery?

We sometimes think of memory behaving like a muscle. It's a useful, imperfect analogy.
The power of recall can be strengthened and improved with training (to an extent), and it can weaken and atrophy with age, neglect, or injury.
Like our capacity for making decisions, I believe that memory can get tired throughout each day too.
The late afternoon feeling of weakening concentration might actually be our memory getting tired of lifting information to our consciousness.
This is just my personal intuition, though, I don't have any research to support it.

Someday you may experience a concussion, stroke, aneurysm, coma, childbirth, natural disaster, violent crime, accident, surgery, death of a loved one, or another trauma.
Some people (like the man who went to the dentist) emerge such things with a permanently decimated memory, while others are unchanged or recover eventually.
Even if you manage to live a long carefree life, you still face an increasing risk of subjective cognitive decline as you age.[^2] 
You may not realize how much your memory has gradually deteriorated, or you may have forgotten how good your memory once was.

### Experiment Results

To that end, before continuing, let's see how well you memorized the short list of words from the beginning.
Jot down the words you can remember, and [then go back to the test at the top](#the-importance-of-memory) and compare.
Here's a text box you can use:

<textarea style="width:100%; font: inherit;"></textarea>

How did you do?
Even healthy young people have trouble accurately remembering a random list of words in the best circumstances.
I'm certain you got at least one wrong.

::: details Here's why...
I cheated. The test above randomly swaps out one word if you leave it alone for 3 seconds; 
[here's the code](https://gitlab.com/barnabas/barnabas.gitlab.io/-/blob/master/docs/.vuepress/components/MemoryTest.vue).
Sorry! But the idea is to give a sense of what an unreliable memory feels like.
Consider it a preview of the future.
:::

## What Office Workers Memorize

If you work at an office, you rely on your memory extensively throughout the day.
You might easily recall a sequence of a dozen small facts over an hour or so each day in order to successfully navigate to your desk.
How do you get to your office? Where is the building entrance? On which floor do you work? Where is your desk? etc.
Every once in a while you might find yourself taking the wrong exit, boarding the wrong bus, or getting off the elevator at the wrong floor, and this lays bare the complexity of your daily dance.

When you sit down at your desk, you'll have to remember how to log in and to unlock your computer.
For security purposes, you might have to enter your username in a text box rather than pick it from a list.
It might seem trivial to remember a something based on your own name. Who could possibly forget their own name? 
Your real name might be too long for the system, or can't be typed on your company's keyboards, or might have legally changed.
You might have logins for systems with differing username formats: first name dot last name, first initial + last name, and so on.
You may have multiple logins for one system (`username2`, `username_admin`, etc.) depending on the level of system access you need.
The point is, there are in fact many ways to get your username wrong.

As for passwords, organizations often enforce complexity and expiration policies.
You might cope with that by appending an incrementing number to the end of the same password.
That means remembering not just the password itself, but which version of your password you're currently on.
"Wait, am I on `MyC00lPassword3` or `MyC00lPassword4`?" you'll ask yourself.
Password managers are no help when first logging in to your own computer.

Because weak or reused passwords are bad, more organizations require two-factor authentication.
That means entering a one-time code from a hardware device, a smartphone app, or a text message.
Of course, you'll have to remember what to do next; the login screen will probably not tell you where to get the code.
If the code is on your phone, you might have to first remember that and enter your phone unlock code to open it, unless your face or fingerprint will do.
You might repeat this login process several times throughout the day, depending on the paranoia of your IT department.

Once you've managed to find your desk and sign in to your computer, you might begin your workday with the unread messages in your email inbox.
As you steadily process each unread message in turn, you recall who the sender is, their relationship to you, your past interactions, and so on.
Some teams discuss and track decisions in other systems like chat applications, collaborative wikis, project trackers, digital whiteboards, etc.
You must remember what system your team is currently using and how they use it.
If you deal with other teams, you might have to learn their preferred communication tools and styles too.
That can get confusing as teams try out new ways to communicate and lose interest in old ways.

Throughout the day, you depend on your memory to stay updated on what you're doing and what has changed.
Beyond just the mechanics of the job, there is the social information that requires some of your memory's effort.
Your coworkers each have particular strengths, sensitivities, ambitions, and interests.
It's considered polite to take a degree of interest in one other's personal lives and make appropriate concessions when those events affect mood or performance.

## What Software Engineers Memorize

I can only speak from my own perspective; I'd be interested to read similar descriptions for different professions.
While many developers privately admit their best skill is to know how to use a search engine, the more knowledge you can hold in active memory, the more productive you'll be.

The activity of programming itself depends on memorizing syntax of a particular programming language, of course.
When you're learning a new language, you'll repeatedly search for examples on basic things like declaring a variable, defining a function, or the best way to iterate over an array.
Depending on the language, syntax includes how to interact with common library.
For example, to figure out how long a bit of text is, maybe every string object has a `.count` or  `.length` property, or maybe there's a separate `strlen()` function.
It's important to remember the specifics of where things are and how to get things done in that particular language.

Besides working on the code itself, increasingly, software engineers maintain configuration files that affect the compiler or the runtime environment[^3].
Developers often maintain unit test scripts, mock data, and test runner configuration too.
Closely related, there may be scripts and configuration for deployment, such as Terraform scripts, Helm charts, Docker files, and so on.
You'll have to remember that these things exist and learn how they work.

Nearly all programs nowadays make use of third-party open source libraries and frameworks.
For example, JavaScript programmers often need to commit to learning [Vue](./vue.md), React, Angular, or something else.
The fact that each of these frameworks have their own conferences indicates how much there is to know.
There may be a UI component library that works with the framework[^4], and that will bring its own set of standards, documents, and workarounds.

You'll have to keep a mental model of your program or system alive in your memory as you work.
This means understanding how the code will be executed and repeatedly imagining how that will change as you make changes.
You'll have to keep track of the specific functionality you're working on at the moment, but also the broader design.
You may also need to think about the runtime environment, network topology, external systems, and so on.
Oftentimes that means having an understanding of the ins-and-outs of AWS, Microsoft Azure, GCP, or something else.
Don't even get me started about Kubernetes.[^5]

Projects rarely adhere to a single language, so each time you switch between languages, you have to remember the new set of peculiarities and idioms.
Senior engineers and consultants might participate in multiple projects simultaneously, so each additional project is a multiplier of all the above complexity.
Effective engineers use a variety of coping mechanisms[^6] to reduce their memory load:

* use IDEs that provide context-sensitive help, code autocomplete, refactoring tools, and linting
* write unit tests to guard against unintended consequences
* automate as much as they possibly can with shell scripts
* stick with older technologies they know well
* divide up areas of responsibility with other team members
* refactor or replace code and systems they don't understand
* avoid changing jobs or projects too often
* write documentation and call meetings instead of working
* become grumpy, antisocial, uncooperative
* move to a cabin in the wilderness

## Implications and Observations

Reading and writing pages to the book of memory is hard work, and humans tend to avoid unpleasant things.
We also tend to underestimate the effort involved in things we don't understand or notice, especially other people's effort.
If people are fighting to reduce their own mental fatigue, even subconsciously, then some of these behaviors might be the result:

* People resist change because it will devalue the things they've fought hard to learn
* Some treat a new team member coolly to avoid committing to remembering new things about them
* Someone acts inconsiderately because they've avoided learning about another person's cultural expectations
* Engineers create systems with poor usability in order to reduce their own complexity, failing to conserve users' mental energy
* The "Not Invented Here" syndrome becomes a team's way of avoiding learning something new and complicated
* Projects stall as people obsess over minor details that they understand rather than tackling unknowns
* Leaders try to reduce mistakes by excessively establishing procedures, systems, and checklists

It's possible that some counterproductive behaviors are just the result of individuals hoarding their memory energy.
"Things will be much better if I convince everyone to do this one extra thing," we think to ourselves.
If we're being selfish with our mental energy, it's time to recognize it and stop.
If we're the victim of mental hoarding behavior, a constructive response is to help the offender to carry their burden.

Managers reading this might complain that last bullet point is out of place: procedures, systems, and checklists are good!
Air conditioning is also good, up until everyone in the city puts one in their window and switches it on during the hottest day of the year.
Adding procedures without pruning obsolete ones can produce short-term gains, but often have long-term consequences.
If an organization is loading down people's memory circuits with constant changes or pointless systemic complexity, then other kinds of hoarding behaviors might become more common.
In order to make it kinder, more welcoming, agile, open-minded place, leadership would do well take steps to keep the collective cognitive burden as low as possible.

It's important for everyone to empathize with individuals who struggle with memory problems.
We ought to do what we can to make things easier for them and be more understanding when they falter.
We shouldn't be so quick to ask tired people to carry more things.
After all, someday our memory won't be what it once was, and we will appreciate a world that treats memory like a precious, limited resource.

[^1]: On the subject of _déjà vu_ and memory, here's the list of related French feeling terms that I can never seem to remember:           
    * _Jamais vu_ ("never seen"): the feeling of not recognizing a situation that should be familiar
    * _Presque vu_ ("almost seen"): the feeling of being on the brink of an insight or epiphany
    * _Déjà vécu_ ("already living through"): the intense, pathological _déjà vu_ that one acts upon
    * _Déjà rêvé_ ("already dreamed"): the feeling of having already dreamed what one is now experiencing
    * _Déjà entendu_ ("already heard"): the feeling of being sure one has heard something before

[^2]: [Subjective Cognitive Decline](https://www.cdc.gov/aging/aginginfo/subjective-cognitive-decline-brief.html) (SCD) 
    is the self-reported experience of worsening or more frequent confusion or memory loss. 
    It is a form of cognitive impairment and one of the earliest noticeable symptoms of Alzheimer's disease and related dementias.

[^3]: Some examples of configuration files are 
    [EditorConfig](https://editorconfig.org/), [tsconfig.json](https://www.typescriptlang.org/docs/handbook/tsconfig-json.html), 
    [ESLint config](https://eslint.org/docs/user-guide/configuring), [Webpack config](https://webpack.js.org/configuration/), 
    [appsettings.json](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-3.1#appsettingsjson).
    
[^4]: Some examples of [Material Design](https://material.io/) UI frameworks are 
    [Vuetify](https://vuetifyjs.com/en/), 
    [Material-UI](https://material-ui.com/),
    and [Angular Material](https://material.angular.io/).
    [Bootstrap](https://getbootstrap.com/) based frameworks include 
    [BootstrapVue](https://bootstrap-vue.org/),
    [React-Bootstrap](https://react-bootstrap.github.io/),
    and [ng-bootstrap](https://ng-bootstrap.github.io/).
    
[^5]: Seriously, don't.

[^6]: Just because it's a common coping mechanism doesn't mean that it's a good thing that I recommend doing.
