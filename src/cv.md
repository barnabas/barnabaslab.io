---
title: Résumé
navbar: false
sidebarDepth: 2
---
# [Barnabas Kendall](/)

I like solving business and technical problems using my experience and intuition, and training others to do the same.
I know how to listen carefully and learn quickly. 
I understand how complex applications are made, how they break, and how to fix them.
I'm happiest when I can help people work better and make technology simpler.

I have real-world work experience with many technologies, including: 
JavaScript/TypeScript (Node.js, Express, Vue, React, AngularJS, jQuery), 
.NET (ASP.NET/C#), Docker, Kubernetes, Helm, Terraform, Demandware, PHP, Java, Python,
database design and administration (PostgreSQL/PostGIS, SQL Server, Oracle, MySQL),
various cloud providers (Google Cloud, AWS, Azure, Cloudflare, Heroku, Vercel)
and cloud-native technologies (RDS, EKS, DynamoDB, S3, SQS, SNS).

I have done full life cycle project planning, resource management, budgeting, virtual teams/offshore, technical user support, documentation, training, and mentoring. 
I've worked in a variety of industries such as tax, analytics, consulting, mobile, eCommerce, web search, energy, mortgage, pharmaceuticals, and casual games.

## Recent Experience

### _International NPO_

#### Software Engineer / Support

- May 2020 -- Present
- Remote

I've been working almost full-time as a remote volunteer for an international nonprofit organization on a team responsible for internal infrastructure.
My team's project supports a larger technical organization transitioning from on-premise service hosting to cloud providers and Kubernetes.
Roughly a quarter of my time involves full-stack development work using TypeScript, Node.js, Express, Vue, C#, and Python.
Another quarter of my time is spent working on scripts in HCL (Terraform) or bash, or Helm charts and Docker files to manage Kubernetes or other cloud infrastructure.
The remainder of my time is spent in meetings, code reviews, research, documentation, threat-modeling, training, and user-support duties.

### Avalara

#### Senior Software Engineer, UX

- March 2015 -- February 2020
- Seattle, Washington / Remote

I was the technical and UX lead on an internal tool for managing tax content.
The product was called [Content Central](articles/content-central.md) and was built with Node, Express, Postgres/PostGIS, and Vue. 
I did user needs analysis, storyboarding, wireframe mockups, static prototypes, documentation, training, and videos. 
I architected a web application with a static front end built with Angular/Webpack/Gulp and dynamic backend API originally in ASP.NET, later in Node.js/Express. 
I integrated the product with Okta to enable SSO for our internal user base. 
I implemented a geographic boundary editor.
In 2018, I proposed and led the successful implementation of a gradual rewrite from Angular to Vue. 
As a team lead, I spend a significant amount of time in code reviews and other training.

### Scout by ServiceSource

#### Senior Web Application Developer

- September 2012 -- March 2015
- Issaquah, Washington

I was the team lead of three web developers, and I worked closely with the product manager to design and implement new features, enhancements, and bug fixes to our customer-facing web application and some internal web applications.
- Responsible for developing the company’s SaaS web application, “Optimizer”
- Responsible for maintaining the internal management web UI “Operations”
- Contributed to maintenance of company's legacy MLS product
- Spearheaded rewrite of Optimizer and Operations over from prototype to production using ASP.NET MVC4, Web API, AngularJS and other technologies
- Worked on a project to re-contextualize Optimizer as a Salesforce canvas app, including Salesforce OAuth for SSO with our own internal authentication

### Live Area Labs

#### eCommerce Developer

- April 2012 – September 2012
- Seattle, Washington

I developed and managed the user experience and administrative sections of several large eCommerce stores (including Fila, Brooks, LUSH, Urban Decay), as well as other projects using various CMS. 
Working with a talented graphic design and branding team, I translated their art into function using responsive web technologies. 
I often supplemented an on-site development team as an advisor and technical architect, or solved eCommerce outages or production issues.
I was able to contribute to implementing coding, issue tracking, and source control procedures, specifically by introducing and managing the team's transition to Assembla. 
I also spearheaded a developer brown-bag lunch to discuss technology topics.

## Prior Experience

- [Blue Nile](https://www.bluenile.com/): August 2011 – April 2012
- Independent consultant: January 2008 – August 2011
- Greenlight Wireless (defunct): August 2004 – December 2007
- [Commerce Velocity](http://www.cvelocity.com/): 2001 -- 2003
- Software Architects (now [Sogeti](https://www.sogeti.com/)): 2000 -- 2001
- AltaVista Shopping.com: 1998 -- 2000
