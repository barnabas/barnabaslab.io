---
home: true
heroImage: ./headshot.webp
tagline: Welcome to my personal website.
actions:
  - text: Résumé
    link: /cv.html
    type: secondary

footer: Copyright © 2022 Barnabas Kendall
---

The latest blog post is <LatestLink link-type="blogPost" />, 
and the latest article is <LatestLink link-type="article" />.

My email address is my first name dot my last name at gmail.
I have profiles elsewhere:

- [GitLab](https://gitlab.com/barnabas)
- [GitHub](https://github.com/barnabas)
- [LinkedIn](https://www.linkedin.com/in/barnabas/)
- [Twitter](https://twitter.com/BarnabasK)
- [Stack Overflow](https://stackoverflow.com/users/53182/barnabas-kendall)
